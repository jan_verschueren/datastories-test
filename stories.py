# -*- coding: utf-8 -*-
import os
import hashlib
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime


# create the flask app
app = Flask(__name__)
# fetch config from stories.cfg file
app.config.from_pyfile('stories.cfg')
# initialize sqlAlchemy
db = SQLAlchemy(app)

#define User model
class User(db.Model):
    id = db.Column('id', db.Integer, primary_key=True)
    username = db.Column(db.String(50))
    first_name = db.Column(db.String(50))
    last_name = db.Column(db.String(100))
    password = db.Column(db.String(32))
    role = db.Column(db.String(20))


    def __init__(self, username, first_name, last_name, password, role):
        self.username = username
        self.password = password
        self.first_name = first_name
        self.last_name = last_name
        self.role = role

#define Story model      
class Story(db.Model):
    id = db.Column('id', db.Integer, primary_key=True)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    creation_date = db.Column(db.DateTime)
    title = db.Column(db.String(50))
    body = db.Column(db.Text)
    status = db.Column(db.String(20))
    author = db.relationship('User', foreign_keys='Story.author_id')

    def __init__(self, title, body, author_id):
        self.title = title
        self.body = body
        self.author_id = author_id
        self.creation_date = datetime.utcnow()
        self.status = 'published'

#initializes data. Drops all content and recreates the initial demo content
def init_db():
    """Initializes the database."""
    db.drop_all()
    db.create_all()
    defaultPassword = hashlib.md5(('demo' + app.config['SALT']).encode('utf-8'))
    user1 = User('admin', 'Jan', 'Verschueren', defaultPassword.hexdigest(), 'admin');
    db.session.add(user1)
    
    user2 = User('johndoe', 'John', 'Doe', defaultPassword.hexdigest(), 'author');
    db.session.add(user2)
    db.session.flush()
    db.session.refresh(user2)
    
    story = Story('Coincidenceville', 'Seventeen women from ages twenty to fifty-seven wore the same red dress to the Westbury Country Club dance. Kelsey got fired by the boss who canned her sister Stacey, ten years and two states apart. Johnny&#39;s pocket contained $1.50, exactly the right amount to buy an orange popsicle from the Good Humor truck. All the miners in Bim, West Virginia, have the same last name. Professors Leonard and Daniels have been teaching identical courses since 2001. Your coincidence turns out to be just like mine. This world ends just as life on another planet, six million light years away, begins.', user2.id)
    db.session.add(story)
    
    story = Story('The Postman', 'I&#39;m surprised to see the postman&#39;s face at last. The wrinkles parenthesizing his lips and eyes are soft, instead of creased with fury as I had imagined. I expected an ogre, a baba yaga, because of the way his shadow always dropped my packages on the ground and kicked them - banging on the door violently before vanishing like a ghost. I stood by the window determined to "shoo" him. But now that I&#39;ve caught him I realize that he is just a man. Mild. Meek. Holding my name on his tongue while the arm in his bag reaches for another.', user2.id)
    db.session.add(story)
    
    db.session.commit()

#run on command initdb to initialize database
@app.cli.command('initdb')
def initdb_command():
    """Creates the database tables."""
    init_db()
    print('Initialized the database.')


#on default route, show list of all published stories
@app.route('/')
def show_stories():
    stories = Story.query.filter_by(status='published')
    return render_template('show_stories.html', stories=stories)

#define story detail route. Show full story on this page
@app.route('/story/<int:story_id>')
def story_detail(story_id):
    story = Story.query.filter_by(status = 'published', id = story_id).first()
    loggedInUser = User.query.filter_by(id=session.get('user_id')).first()
    return render_template('story_detail.html', story=story,loggedInUser=loggedInUser)   

#add story based on post variables
@app.route('/add', methods=['POST'])
def add_story():
    #if not logged in, abort with 401 header code
    if not session.get('logged_in'):
        abort(401)
    
    story = Story(request.form['title'], request.form['text'], session.get('user_id'))
    db.session.add(story)
    db.session.commit()

    flash('New story was successfully posted')
    return redirect(url_for('show_stories'))

#delete story by id    
@app.route('/delete/<int:story_id>')
def delete_story(story_id):
    #abort if user is not logged in
    if not session.get('logged_in'):
        abort(401)
    
    #abort if user is no admin
    user = User.query.filter_by(id=session.get('user_id')).first()
    if user.role != 'admin':
        abort(401)
        
    db.session.query(Story).filter(Story.id == story_id).delete(synchronize_session='evaluate')
    db.session.commit()
    return redirect(url_for('show_stories'))
    
#login route
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    #if form is submitted
    if request.method == 'POST':
        hashedPassword = hashlib.md5(request.form['password'].encode('utf-8') + app.config['SALT'].encode('utf-8'))
        user = User.query.filter_by(username=request.form['username'], password=hashedPassword.hexdigest()).first()

        if user == None:
            error = 'Invalid username or password'
        else:
            session['logged_in'] = True
            session['user_id'] = user.id
            flash('You were logged in')
            return redirect(url_for('show_stories'))

    return render_template('login.html', error=error)

#logout route. remove session vars and go to homepage
@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    session.pop('user_id', None)
    flash('You were logged out')
    return redirect(url_for('show_stories'))
